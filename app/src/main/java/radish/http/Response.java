package radish.http;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Response {
    public int status;
    public String message;
    public ArrayList<Byte> rawMessage;

    public Response(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public Response(int status, ArrayList<Byte> rawMessage) {
        this.status = status;
        this.rawMessage = rawMessage;
    }

    public JSONObject getMessageAsJSONObject() throws JSONException {
        return new JSONObject(message);
    }

    public JSONArray getMessageAsJSONArray() throws JSONException {
        return new JSONArray(message);
    }

    public Bitmap getMessageAsBitmap() {

        byte[] bytesArray = new byte[rawMessage.size()];
        int i = 0;
        for (Byte b : rawMessage) {
            bytesArray[i] = b;
            i++;
        }
        return BitmapFactory.decodeByteArray(bytesArray, 0, bytesArray.length);
    }
}
