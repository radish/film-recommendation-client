package radish.http;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import radish.film_recommendation.UserSession;

public class RequestWrapper<CallbackClassType> extends AsyncTask<JSONObject, Void, Response> {
    protected static final String TAG = "RequestWrapper";

    protected HttpURLConnection connection;
    protected Method callback = null;
    protected CallbackClassType callbackClassInstance = null;
    protected boolean raw = false;

    public RequestWrapper(URL url, String method) throws IOException {
        this(url, method, false, false);
    }

    public RequestWrapper(URL url, String method, boolean raw, boolean anonymous) throws IOException {
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        this.raw = raw;

        if (!raw) {
            connection.setRequestProperty("Content-Type", "application/json");
        }

        String token = UserSession.getUserToken();
        if ( !anonymous && token != null ) {
            connection.setRequestProperty("Authorization", token);
        }

        if (!method.equals("GET")) {
            connection.setDoOutput(true);
        }
    }

    public void setCallback(Method callback) {
        this.callback = callback;
    }

    public void setCallback(Method callback, CallbackClassType callbackClassInstance) {
        this.callback = callback;
        this.callbackClassInstance = callbackClassInstance;
    }

    @Override
    protected Response doInBackground(JSONObject ... params) {
        try {
            if (params.length != 0) {
                JSONObject data = params[0];

                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(data.toString());
                dStream.flush();
                dStream.close();
            }
            int responseCode = connection.getResponseCode();

            if (200 <= responseCode && 299 >= responseCode) {
                if (raw) {
                    ArrayList<Byte> responseOutputRaw = readRawResponse(connection.getInputStream());
                    return new Response(responseCode, responseOutputRaw);
                } else {
                    String responseOutput = readTextResponse(connection.getInputStream());
                    return new Response(responseCode, responseOutput);
                }
            } else {
                String responseOutput = readTextResponse(connection.getErrorStream());
                return new Response(responseCode, responseOutput);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Response response) {
        if (callback != null) {
            try {
                if (callbackClassInstance != null) {
                    callback.invoke(callbackClassInstance, response);
                } else {
                    callback.invoke(response);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                Log.e(TAG, "Cannot call response callback: " + e.getLocalizedMessage());
            }
        }
    }

    private ArrayList<Byte> readRawResponse(InputStream inputStream) throws IOException {
        BufferedInputStream braw;
        braw = new BufferedInputStream(inputStream);
        ArrayList<Byte> responseOutputRaw = new ArrayList<Byte>();
        int read_byte;
        while ((read_byte = braw.read()) != -1) {
            responseOutputRaw.add((byte) read_byte);
        }
        braw.close();
        return responseOutputRaw;
    }

    private String readTextResponse(InputStream inputStream) throws IOException {
        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder responseOutput = new StringBuilder();
        while ((line = br.readLine()) != null) {
            responseOutput.append(line);
        }
        br.close();
        return responseOutput.toString();
    }
}
