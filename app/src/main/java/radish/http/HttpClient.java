package radish.http;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class HttpClient {
    private URL url;

    public HttpClient(String endpoint_url) throws MalformedURLException {
        url = new URL(endpoint_url);
    }

    public Response get() throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<Object> getWrapper = new RequestWrapper<>(url, "GET");

        getWrapper.execute();
        return getWrapper.get();
    }

    public void get(Method callback) throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<Object> getWrapper = new RequestWrapper<>(url, "GET");

        getWrapper.setCallback(callback);
        getWrapper.execute();
    }

    public <CallbackClassType> void get(Method callback, CallbackClassType instance)
            throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<CallbackClassType> getWrapper = new RequestWrapper<>(url, "GET");

        getWrapper.setCallback(callback, instance);
        getWrapper.execute();
    }

    public void getImage(Method callback) throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<Object> getWrapper = new RequestWrapper<>(url, "GET", true, true);

        getWrapper.setCallback(callback);
        getWrapper.execute();
    }

    public <CallbackClassType> void getImage(Method callback, CallbackClassType instance)
            throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<CallbackClassType> getWrapper = new RequestWrapper<>(url, "GET", true, true);

        getWrapper.setCallback(callback, instance);
        getWrapper.execute();
    }
    public Response post(JSONObject data)
            throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<Object> postWrapper = new RequestWrapper<>(url, "POST");

        postWrapper.execute(data);
        return postWrapper.get();
    }

    public void post(JSONObject data, Method callback)
            throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<Object> postWrapper = new RequestWrapper<>(url, "POST");

        postWrapper.setCallback(callback);
        postWrapper.execute(data);
    }

    public <CallbackClassType> void post(JSONObject data, Method callback,
                                         CallbackClassType instance)
            throws InterruptedException, ExecutionException, IOException {
        RequestWrapper<CallbackClassType> postWrapper = new RequestWrapper<>(url, "POST");

        postWrapper.setCallback(callback, instance);
        postWrapper.execute(data);
    }

}
