package radish.film_recommendation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import radish.carousel.CarouselActivity;
import radish.http.HttpClient;
import radish.http.Response;
import radish.models.Urls;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    private Context context;
    private HttpClient http_client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = getApplicationContext();
        MobileAds.initialize(context, "ca-app-pub-3872092513732495~4884074968");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        try {
            mAdView.loadAd(adRequest);
        } catch (NullPointerException e) {
            // No internet permission probably
            // It seems there is nothing we can do here
        }

        try {
            http_client = new HttpClient(Urls.services.get("Login"));
        } catch (IOException e) {
            Log.e(TAG, "Malformed URL provided");
        }

        setEvents();
    }

    private void validateData(String email, String password) throws Exception {
        if (email.length() == 0)
            throw new Exception("Email cannot be empty.");
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            throw new Exception("Wrong email format.");
        if (password.length() < 6)
            throw new Exception("Password cannot be empty or shorter than six characters.");
    }

    private void setEvents() {
        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

        TextView signUpTextView = (TextView) findViewById(R.id.signUpTextView);
        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, RegisterActivity.class);

                startActivity(i);
            }
        });

        TextView privacyTextView = (TextView) findViewById(R.id.privacyTextView);
        privacyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PrivacyActivity.class);
                startActivity(i);
            }
        });
    }

    private void loginUser() {
        try {
            String email = ((EditText) findViewById(R.id.emailEditText)).getText().toString();
            String password = ((EditText) findViewById(R.id.passwordEditText)).getText().toString();

            validateData(email, password);
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("password", password);


            http_client.<LoginActivity>post(
                    data, this.getClass().getDeclaredMethod("onHttpResponse", Response.class), this
            );
        } catch (Exception e) {
            Log.e(TAG, e.getClass().getName());
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onHttpResponse(Response response) {
        if (response.status == HttpURLConnection.HTTP_OK) {
            try {
                JSONObject result = response.getMessageAsJSONObject();

                String token = result.getString("token");
                UserSession userSession = new UserSession(context);
                userSession.createLoginSession(token);

                if (userSession.isAuthenticated()) {
                    Intent i = new Intent(context, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown response", Toast.LENGTH_LONG).show();
            }
        } else {
            try {
                JSONObject error = response.getMessageAsJSONObject();
                Toast.makeText(context, error.getString("detail"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown server error", Toast.LENGTH_LONG).show();
            }
        }
    }
}
