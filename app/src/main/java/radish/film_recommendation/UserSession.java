package radish.film_recommendation;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class UserSession {
    static private SharedPreferences preferences;
    private Context context;

    private static String token = null;

    private static final String PREF_NAME = "fre_user";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String FRE_TOKEN = "fre_token";

    public UserSession(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public boolean isAuthenticated() {
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void createLoginSession(String token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(FRE_TOKEN, token);
        editor.apply();

        UserSession.token = token;
    }

    public void checkLogin() {
        // Check login status
        if(!this.isAuthenticated()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);
        }
    }

    static public String getUserToken()
    {
        return preferences.getString(FRE_TOKEN, null);
    }

    public void logoutUser() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

        UserSession.token = null;

        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }
}
