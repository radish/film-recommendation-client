package radish.film_recommendation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.*;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import radish.http.HttpClient;
import radish.http.Response;
import radish.models.Film;
import radish.models.Urls;

/**
 * Created by Bartosz Litwiniuk on 14.06.2016.
 */
public class SearchfilmActivity extends AppCompatActivity {

    private Context context;
    private static final String TAG = "SearchfilmActivity";
    private ArrayAdapter<String> adapter;
    String titles[];
    private ArrayList<Film> films = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();
        UserSession userSession = new UserSession(context);

        if (!userSession.isAuthenticated()) {
            Intent i = new Intent(context, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(i);
            finish();
        }

        setContentView(R.layout.activity_searchfilm);

        setEvents();
    }

    private void initListView() {
        ListView listView = (ListView) findViewById(R.id.TitlesListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                fillFilmInformation(position);
            }
        });
        final ArrayList<String> list = new ArrayList<String>();
        if (titles == null || titles.length == 0)
            return;
        for (int i = 0; i < titles.length; i++) {
            list.add(titles[i]);
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(context,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    private void setEvents() {
        Button searchButton = (Button) findViewById(R.id.searchFilmsButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchButtonEvent();
            }
        });
    }

    private void fillFilmInformation(int position) {
        Film film = films.get(position);
        ImageView imgview = (ImageView) findViewById(R.id.filmImageView);
        imgview.setImageDrawable(null);

        HttpClient http_client;
        try {
            http_client = new HttpClient(film.poster);
            http_client.getImage(this.getClass().getDeclaredMethod("onDownloadImage", Response.class), this);
        } catch (IOException e) {
            Log.e(TAG, "Malformed URL provided");
        } catch (Exception e) {
            Log.e(TAG, e.getClass().getName());
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
        TextView yearTextView = (TextView) findViewById(R.id.year);
        TextView genresTextView = (TextView) findViewById(R.id.genres);
        TextView plotTextView = (TextView) findViewById(R.id.plot);
        TextView actorsTextView = (TextView) findViewById(R.id.actors);
        TextView writersTextView = (TextView) findViewById(R.id.writers);
        TextView directorsTextView = (TextView) findViewById(R.id.directors);
        TextView imdbTextView = (TextView) findViewById(R.id.imdbId);
        TextView movieLensIdTextView = (TextView) findViewById(R.id.movieLensId);
        TextView votesNumberTextView = (TextView) findViewById(R.id.votesNumber);
        TextView votesPositionTextView = (TextView) findViewById(R.id.votesPosition);


        titleTextView.setText(film.title);
        yearTextView.setText(film.year);
        genresTextView.setText(film.genres.toString());
        plotTextView.setText(film.plot);
        actorsTextView.setText(film.actors.toString());
        writersTextView.setText(film.writers.toString());
        directorsTextView.setText(film.directors.toString());
        imdbTextView.setText(film.imdb_id);
        movieLensIdTextView.setText(String.valueOf(film.movielens_id));
        votesNumberTextView.setText(String.valueOf(film.votes_number));
        votesPositionTextView.setText(String.valueOf(film.votes_position));
    }

    public void onDownloadImage(Response response) {
        ImageView image = (ImageView) findViewById(R.id.filmImageView);
        image.setImageBitmap(response.getMessageAsBitmap());
    }

    private void searchButtonEvent() {
        EditText titleEditView = (EditText) findViewById(R.id.titleEditText);
        if (titleEditView.getText().length() < 4) {
            Toast.makeText(context, "Input cannot be shorter than 4 characters!", Toast.LENGTH_LONG).show();
            return;
        } else {
            String filmName = titleEditView.getText().toString().replace(' ', '_');
            String url = Urls.services.get("SearchFilms") + filmName;

            HttpClient http_client;

            try {
                http_client = new HttpClient(url);
                http_client.get(getClass().getDeclaredMethod("onHttpResponse", Response.class), this);
            } catch (IOException e) {
                Log.e(TAG, "Malformed URL provided");
            } catch (Exception e) {
                Log.e(TAG, e.getClass().getName());
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }

    public void onHttpResponse(Response response) {
        if (response.status == HttpURLConnection.HTTP_OK) {
            try {
                JSONArray result = response.getMessageAsJSONArray();
                String filmTitles[] = new String[result.length()];
                films.clear();
                for (int i = 0; i < result.length(); i++) {
                    Gson gson = new GsonBuilder().create();
                    Film film = gson.fromJson(result.getJSONObject(i).toString(), Film.class);
                    filmTitles[i] = film.title;
                    films.add(film);
                }
                titles = filmTitles;
                if (titles.length == 0) {
                    Toast.makeText(context, "Zero results", Toast.LENGTH_LONG).show();
                    return;
                }
                initListView();
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown response", Toast.LENGTH_LONG).show();
            }
        } else {
            try {
                JSONObject error = response.getMessageAsJSONObject();
                Toast.makeText(context, error.getString("detail"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown server error", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}



