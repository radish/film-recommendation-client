package radish.film_recommendation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import radish.carousel.CarouselActivity;

public class MainActivity extends AppCompatActivity {

    private Context context;
    private String activityType = "ActivityType";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        UserSession userSession = new UserSession(context);

        if (!userSession.isAuthenticated()) {
            Intent i = new Intent(context, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(i);
            finish();
        }

        setContentView(R.layout.activity_main);
        setEvents();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        context = getApplicationContext();
        UserSession userSession = new UserSession(context);
    }

    private void setEvents(){
        ImageButton logoutButton = (ImageButton) findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        ImageButton exploreFilmsButton = (ImageButton) findViewById(R.id.exploreFilmsButton);
        exploreFilmsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, FilminfoActivity.class);
                startActivity(i);
            }
        });

        ImageButton carousel =  (ImageButton) findViewById(R.id.carouselButton);
        carousel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CarouselActivity.class);
                i.putExtra(activityType, "Initial rating");
                startActivity(i);
            }
        });

        ImageButton savedFilms = (ImageButton) findViewById(R.id.savedFilmsButton);
        savedFilms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CarouselActivity.class);
                i.putExtra(activityType, "Saved films");
                startActivity(i);
            }
        });

        ImageButton recommendedFilms = (ImageButton) findViewById(R.id.recommendedFilmsButton);
        recommendedFilms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CarouselActivity.class);
                i.putExtra(activityType, "Recommended films");
                startActivity(i);
            }
        });

        ImageButton searchFilms = (ImageButton) findViewById(R.id.searchFilms);
        searchFilms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SearchfilmActivity.class);
                startActivity(i);
            }
        });

    }

    private void logoutUser(){
        UserSession userSession = new UserSession(getApplicationContext());
        userSession.logoutUser();
    }
}
