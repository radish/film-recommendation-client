package radish.film_recommendation;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import radish.http.HttpClient;
import radish.http.Response;
import radish.models.Urls;

public class FilminfoActivity extends AppCompatActivity {
    private static final String TAG = "FilminfoActivity";

    InterstitialAd interstitialAd;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filminfo);

        context = getApplicationContext();

        interstitialAd = new InterstitialAd(getApplicationContext());
        interstitialAd.setAdUnitId("ca-app-pub-3872092513732495/7917619765");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        AdRequest.Builder publisherAdRequestBuilder = new AdRequest.Builder();
        publisherAdRequestBuilder.addKeyword("film")
                .addKeyword("movie");
        interstitialAd.loadAd(publisherAdRequestBuilder.build());
        setEvents();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().addKeyword("film")
                .addKeyword("movie")
                .build();

        interstitialAd.loadAd(adRequest);
    }

    private void setEvents() {
        Button loginButton = (Button) findViewById(R.id.showButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilm();
            }
        });
    }

    private void showFilm() {
        if (interstitialAd.isLoaded())
            interstitialAd.show();
        else {
            AdRequest.Builder publisherAdRequestBuilder = new AdRequest.Builder();
            publisherAdRequestBuilder.addKeyword("film")
                    .addKeyword("movie");
            interstitialAd.loadAd(publisherAdRequestBuilder.build());
            interstitialAd.show();
        }
        EditText filmIdText = (EditText) findViewById(R.id.filmIdEditText);
        String filmId = filmIdText.getText().toString();
        String url = Urls.services.get("FilmInfo") + filmId;
        HttpClient http_client;

        try {
            http_client = new HttpClient(url);
            http_client.get(this.getClass().getDeclaredMethod("onHttpResponse", Response.class), this);
        } catch (IOException e) {
            Log.e(TAG, "Malformed URL provided");
        } catch (Exception e) {
            Log.e(TAG, e.getClass().getName());
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onHttpResponse(Response response) {
        ImageView imgview = (ImageView) findViewById(R.id.filmImageView);
        TextView txtview = (TextView) findViewById(R.id.filmTextView);
        if (response.status == HttpURLConnection.HTTP_OK) {
            try {
                JSONObject result = response.getMessageAsJSONObject();

                // clear text and image
                txtview.setText("");
                imgview.setImageDrawable(null);

                String title = result.getString("title");
                String year = result.getString("year");
                String actors = "";
                JSONArray actorsArray = result.getJSONArray("actors");

                if (actorsArray.length() > 0) {
                    actors = actorsArray.getString(0);
                    for (int i = 1; i < actorsArray.length() - 1; i++) {
                        actors += ", " + actorsArray.getString(i);
                    }
                }

                String plot = result.getString("plot");
                String imageurl = result.getString("poster");

                txtview.setText("Title: " + title + "\n" +
                        "Year: " + year + "\n" +
                        "Actors: " + actors + "\n" +
                        "Plot: " + plot);

                HttpClient http_client;
                try {
                    http_client = new HttpClient(imageurl);
                    http_client.getImage(this.getClass().getDeclaredMethod("onDownloadImage", Response.class), this);
                } catch (IOException e) {
                    Log.e(TAG, "Malformed URL provided");
                } catch (Exception e) {
                    Log.e(TAG, e.getClass().getName());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                Toast.makeText(context, "Unknown response", Toast.LENGTH_LONG).show();
            }
        } else {
            try {
                JSONObject error = response.getMessageAsJSONObject();
                Toast.makeText(context, error.getString("detail"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown server error", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onDownloadImage(Response response) {
        ImageView image = (ImageView) findViewById(R.id.filmImageView);
        image.setImageBitmap(response.getMessageAsBitmap());
    }
}

