package radish.film_recommendation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import radish.http.HttpClient;
import radish.http.Response;
import radish.models.Urls;


public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "RegisterActivity";

    private Context context;
    private HttpClient http_client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        context = getApplicationContext();

        try {
            http_client = new HttpClient(Urls.services.get("Register"));
        } catch (IOException e) {
            Log.e(TAG, "IO exception during creating HttpClient");
        }

        setEvents();
    }

    private void registerUser() {
        try {
            String email = ((EditText) findViewById(R.id.emailEditText)).getText().toString();
            String password = ((EditText) findViewById(R.id.passwordEditText)).getText().toString();

            validateData(email, password);
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("password", password);

            http_client.<RegisterActivity>post(
                    data, this.getClass().getDeclaredMethod("onHttpResponse", Response.class), this
            );
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onHttpResponse(Response response) {
        if (response.status == HttpURLConnection.HTTP_CREATED) {
            Toast.makeText(context, "Registered successfully", Toast.LENGTH_LONG).show();
        } else {
            try {
                JSONObject error = response.getMessageAsJSONObject();
                Toast.makeText(context, error.getString("detail"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown server error", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void validateData(String email, String password) throws Exception {
        if (email.length() == 0)
            throw new Exception("Email cannot be empty.");
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            throw new Exception("Wrong email format.");
        if (password.length() < 6)
            throw new Exception("Password cannot be empty or shorter than six characters.");
    }

    private void setEvents() {
        Button registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        TextView signInTextView = (TextView) findViewById(R.id.signInTextView);
        signInTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);

                startActivity(i);
            }
        });
    }
}
