package radish.models;

import java.util.Dictionary;

import java.util.HashMap;
import java.util.Map;

public class Urls {
    private static String host = "http://refrigerator.rivi.pl:8000";
    public static Map<String, String> services = new HashMap<String, String>()
    {
        {
            put("Recommended films", host + "/v0/recommendation/recommended_films");
            put("Saved films", host + "/v0/recommendation/save_films");
            put("Initial rating", host + "/v0/recommendation/initial_ratings");
            put("Rated films", host + "/v0/recommendation/rate_films");
            put("FilmInfo", host + "/v0/film/");
            put("SearchFilms", host + "/v0/search_film/");
            put("Login", host + "/v0/auth/login");
            put("Register", host + "/v0/auth/register");

        }
    };
}
