package radish.models;

import java.util.Dictionary;
import java.util.List;

public class Film {
    public List<String> actors;
    public List<String> directors;
    public List<String> genres;
    public String imdb_id;
    public int movielens_id;
    public String plot;
    public String poster;
    public String title;
    public int votes_number;
    public int votes_position;
    public List<String> writers;
    public String year;

    public Film()
    {

    }
}

