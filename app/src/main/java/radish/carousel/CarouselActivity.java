package radish.carousel;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import radish.film_recommendation.FilminfoActivity;
import radish.film_recommendation.MainActivity;
import radish.film_recommendation.R;
import radish.http.HttpClient;
import radish.http.Response;

import me.crosswall.lib.coverflow.CoverFlow;
import me.crosswall.lib.coverflow.core.PagerContainer;
import radish.models.Film;
import radish.models.FilmVotes;
import radish.models.Urls;

public class CarouselActivity extends FragmentActivity {
    private static final String TAG = "CarouselActivity";
    private Context context;
    private ViewPager pager;
    private PagerAdapter pageAdapter;
    private RatingBar ratingBar;
    private String activityType;
    public Map<Integer, Integer> rates = new HashMap<Integer, Integer>();
    public List<Integer> savedFilms = new ArrayList<Integer>();

    private Integer getCurrentFilmId() {
        Integer id;
        try {
            id = ((CarouselPagerAdapter) pageAdapter).filmsId.get(pager.getCurrentItem());
        } catch (IndexOutOfBoundsException e) {
            id = -1;
        }
        return id;
    }

    private void rateFilms() {
        FilmVotes filmVotes = new FilmVotes();
        filmVotes.film_votes = new ArrayList<Map<String, Integer>>();
        for (Map.Entry<Integer, Integer> entry : rates.entrySet()) {
            Map<String, Integer> map = new HashMap<String, Integer>();
            if (entry.getValue() != 0) {
                map.put("film_id", entry.getKey());
                map.put("rating", entry.getValue());
                filmVotes.film_votes.add(map);
            }
        }
        if (activityType.equals("Recommended films") || activityType.equals("Initial rating")) {
            saveFilms();
        }

        Gson gson = new Gson();
        if (filmVotes.film_votes.size() == 0)
            return;
        String json = gson.toJson(filmVotes, FilmVotes.class);

        String url = Urls.services.get("Rated films");
        HttpClient http_client;

        try {
            JSONObject jsonObject = new JSONObject(json);
            http_client = new HttpClient(url);
            http_client.post(jsonObject, this.getClass().getDeclaredMethod("onRatingComplete", Response.class), this);

            Toast.makeText(context, "Rating complete!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.e(TAG, "Malformed URL provided");
        } catch (Exception e) {
            Log.e(TAG, e.getClass().getName());
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onRatingComplete(Response response) {
        if (response.status == HttpURLConnection.HTTP_OK) {
            Intent i = new Intent(context, MainActivity.class);
            startActivity(i);

        } else {
            try {
                JSONObject error = response.getMessageAsJSONObject();
                Toast.makeText(context, error.getString("detail"), Toast.LENGTH_LONG).show();
                Log.d(TAG, error.toString());
            } catch (JSONException e) {
                Toast.makeText(context, "Unknown server error", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void refreshPage() {
        int filmId = getCurrentFilmId();
        Object element = rates.get(filmId);

        if (element != null) {
            int rate = rates.get(filmId);
            ratingBar.setRating(rate);
        } else {
            ratingBar.setRating(0.0f);
        }

        stylizeSavedButton();
        fillFilmInformation();
    }

    private void fillFilmInformation() {
        int filmId = getCurrentFilmId();
        Film film = ((CarouselPagerAdapter) pageAdapter).films.get(filmId);

        TextView yearTextView = (TextView) findViewById(R.id.year);
        TextView genresTextView = (TextView) findViewById(R.id.genres);
        TextView plotTextView = (TextView) findViewById(R.id.plot);
        TextView actorsTextView = (TextView) findViewById(R.id.actors);
        TextView writersTextView = (TextView) findViewById(R.id.writers);
        TextView directorsTextView = (TextView) findViewById(R.id.directors);
        TextView imdbTextView = (TextView) findViewById(R.id.imdbId);
        TextView movieLensIdTextView = (TextView) findViewById(R.id.movieLensId);
        TextView votesNumberTextView = (TextView) findViewById(R.id.votesNumber);
        TextView votesPositionTextView = (TextView) findViewById(R.id.votesPosition);


        yearTextView.setText(film.year);
        genresTextView.setText(film.genres.toString());
        plotTextView.setText(film.plot);
        actorsTextView.setText(film.actors.toString());
        writersTextView.setText(film.writers.toString());
        directorsTextView.setText(film.directors.toString());
        imdbTextView.setText(film.imdb_id);
        movieLensIdTextView.setText(String.valueOf(film.movielens_id));
        votesNumberTextView.setText(String.valueOf(film.votes_number));
        votesPositionTextView.setText(String.valueOf(film.votes_position));
    }

    private void handleSavedButton() {
        int filmId = getCurrentFilmId();
        if (savedFilms.contains(filmId)) {
            for (int i = 0; i < savedFilms.size(); i++) {
                if (savedFilms.get(i) == filmId) {
                    savedFilms.remove(i);
                    break;
                }
            }
        } else {
            savedFilms.add(filmId);

            ratingBar.setRating(0);
            for (Map.Entry<Integer, Integer> entry : rates.entrySet()) {
                if (entry.getKey() == filmId) {
                    rates.remove(entry.getKey());
                    break;
                }
            }
        }

        stylizeSavedButton();
    }

    private void stylizeSavedButton() {
        int filmId = getCurrentFilmId();
        Button savedButton = (Button) findViewById(R.id.saveButton);
        if (savedFilms.contains(filmId)) {
            savedButton.setBackgroundColor(Color.GREEN);
            savedButton.setText("Saved");
            savedButton.setTextColor(Color.WHITE);
        } else {
            savedButton.setBackgroundColor(Color.WHITE);
            savedButton.setText("Save film");
            savedButton.setTextColor(Color.BLACK);
        }
    }

    private void configRatingBar() {
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setStepSize(1);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                if (fromUser == true) {
                    int filmId = getCurrentFilmId();
                    int rate = (int) ratingBar.getRating();
                    rates.put(filmId, rate);

                    for (int i = 0; i < savedFilms.size(); i++) {
                        if (savedFilms.get(i) == filmId) {
                            savedFilms.remove(i);
                            break;
                        }
                    }
                    stylizeSavedButton();
                }
            }
        });
    }

    private void saveFilms() {
        try {
            if (savedFilms.size() == 0)
                return;
            Gson gson = new Gson();
           /* String json = gson.toJson(savedFilms, List.class);*/
            Map<String, List> map = new HashMap<String, List>() {
                {
                    put("films_saved", savedFilms);
                }
            };

            JSONObject jsonObject = new JSONObject(gson.toJson(map, Map.class));
            HttpClient http_client = new HttpClient(Urls.services.get("Saved films"));
            http_client.post(jsonObject);

        } catch (IOException e) {
            Log.e(TAG, "Malformed URL provided");
        } catch (Exception e) {
            Log.e(TAG, e.getClass().getName());
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Reading passed data from another activity*/
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityType = extras.getString("ActivityType");
        }
        setContentView(R.layout.activity_carousel);
        context = getApplicationContext();
        /*Button events*/
        Button saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSavedButton();
            }
        });
        Button rateButton = (Button) findViewById(R.id.rateFilms);
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateFilms();
            }
        });
        configRatingBar();
        /*Carousel*/
        try {
            pageAdapter = new CarouselPagerAdapter(getFragmentManager(), activityType);
            PagerContainer pagerContainer = (PagerContainer) findViewById(R.id.pager_container);
            pager = pagerContainer.getViewPager();
            pager.setAdapter(pageAdapter);
            pager.setOffscreenPageLimit(15);
            CoverFlow cf = new CoverFlow.Builder()
                    .with(pager)
                    .pagerMargin(-140f)
                    .scale(0.3f)
                    .spaceSize(0f)
                    .rotationY(25f)
                    .build();
        } catch (IOException e) {
            Log.e(TAG, "PagerAdapter can't download data");
        }

        final ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {

                refreshPage();

            }
        };
        pager.addOnPageChangeListener(pageChangeListener);
    }

    public void onFirstPageLoad() {
        refreshPage();
        if (activityType.equals("Saved films")) {
            savedFilms = ((CarouselPagerAdapter) pageAdapter).savedFilms;
            stylizeSavedButton();
        }
    }

    public class CarouselPagerAdapter extends FragmentStatePagerAdapter {
        public ArrayList<Integer> filmsId = new ArrayList<Integer>();
        public ArrayList<String> titles = new ArrayList<String>();
        public ArrayList<String> imageurls = new ArrayList<String>();
        public Map<Integer, Film> films = new HashMap<Integer, Film>();
        public List<Integer> savedFilms = new ArrayList<Integer>();

        public CarouselPagerAdapter(FragmentManager fm, String activityType) throws IOException {
            super(fm);

            String url = Urls.services.get(activityType);
            HttpClient http_client;

            try {
                http_client = new HttpClient(url);
                http_client.get(this.getClass().getDeclaredMethod("onHttpResponse", Response.class), this);
            } catch (IOException e) {
                Log.e(TAG, "Malformed URL provided");
            } catch (Exception e) {
                Log.e(TAG, e.getClass().getName());
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        public void onHttpResponse(Response response) {
            if (response.status == HttpURLConnection.HTTP_OK) {
                try {
                    JSONArray result = response.getMessageAsJSONArray();
                    for (int i = 0; i < result.length(); i++) {
                        Gson gson = new GsonBuilder().create();
                        Film film = gson.fromJson(result.getJSONObject(i).toString(), Film.class);
                        films.put(film.movielens_id, film);
                        filmsId.add(film.movielens_id);
                        titles.add(film.title);
                        imageurls.add(film.poster);
                        notifyDataSetChanged();
                        if (activityType.equals("Saved films")) {
                            savedFilms.add(film.movielens_id);
                        }
                        // invoke onFirstPageLoad manually
                        if (i == 0) {
                            onFirstPageLoad();
                        }
                    }
                } catch (JSONException e) {
                    Toast.makeText(context, "Unknown response", Toast.LENGTH_LONG).show();
                }
            } else {
                try {
                    JSONObject error = response.getMessageAsJSONObject();
                    Toast.makeText(context, error.getString("detail"), Toast.LENGTH_LONG).show();
                    Log.d(TAG, error.toString());
                } catch (JSONException e) {
                    Toast.makeText(context, "Unknown server error", Toast.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public Fragment getItem(int position) {
            return CarouselPageFragment.create(titles.get(position), imageurls.get(position));
        }

        @Override
        public int getCount() {
            return titles.size();
        }
    }

}

