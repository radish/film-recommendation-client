package radish.carousel;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

import radish.film_recommendation.R;
import radish.http.HttpClient;
import radish.http.Response;

public class CarouselPageFragment extends Fragment {
    static final String TAG = "CarouselPageFragment";
    private String title;
    private String imageurl;
    private ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.carousel_page, container, false);

        ((TextView) rootView.findViewById(R.id.carouselTitleText)).setText(title);

        HttpClient http_client;
        try {
            http_client = new HttpClient(imageurl);
            http_client.getImage(this.getClass().getDeclaredMethod("onDownloadImage", Response.class), this);
        } catch (IOException e) {
            Log.e(TAG, "Malformed URL provided");
        } catch (Exception e) {
            Log.e(TAG, e.getClass().getName());
            // it is not good idea to make 15 toast in the same time
            //Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return rootView;
    }

    public void onDownloadImage(Response response) {

        ImageView image = (ImageView) rootView.findViewById(R.id.carouselImageView);
        image.setImageBitmap(response.getMessageAsBitmap());
    }

    public CarouselPageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString("title");
        imageurl = getArguments().getString("imageurl");
    }

    public static CarouselPageFragment create(String title, String imageurl) {
        CarouselPageFragment fragment = new CarouselPageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("imageurl", imageurl);
        fragment.setArguments(args);
        return fragment;
    }
}
